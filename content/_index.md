+++
paginate_by = 2
sort_by = "date"

[extra]
# Enable Katex for post summary in index listing
# if true, this will always load the Katex javascript even if no summary has math!
#math = true
+++
